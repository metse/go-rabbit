package main

import (
	"fmt"
	"log"
	"os"

	"github.com/streadway/amqp"
)

func main() {
	fmt.Println("Go + RabbitMQ Tutorial")
	mqUser := os.Getenv("MQ_USER")
	mqPassword := os.Getenv("MQ_PASSWORD")
	mqHost := os.Getenv("MQ_HOST")
	mqPort := os.Getenv("MQ_PORT")

	connectionString := fmt.Sprintf("amqp://%s:%s@%s:%s/", mqUser, mqPassword, mqHost, mqPort)
	conn, err := amqp.Dial(connectionString)

	if err != nil {
		log.Fatal(err)
		panic(1)
	}

	defer conn.Close()

	fmt.Println("MQ connected!")
}
